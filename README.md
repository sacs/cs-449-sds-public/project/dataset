# Dataset

[data.zip](./data.zip) : Pre-split dataset from the MovieLens project, includes ml-100k and ml-25m.

[data-m2.zip](./data-m2.zip) : Pre-split dataset from the MovieLens project for Milestone 2, includes ml-100k and ml-1m.

## Instructions

1. Download [data.zip](./data.zip) or [data-m2.zip](./data-m2.zip).
2. Unzip and move the included ````ml-...```` directories into the ````data```` directory of your template.

# Updates

Since the original release of Milestone 1, the following have changed:

1. Ratings with values of ````0.5```` have been replaced by ````1.0```` in ````ml-25m/r2.train```` and ````ml-25m/r2.test````, in data.zip to avoid the need for parameterized scaling functions. 

